# Room 1 #
* Assemble/arrange all standalone furniture and decorations
    * assemble chairs.
    * assemble the table.
    * Hang the painting on the wall
    ---- tasks done by you ----
    * purchase items:
        * purchase chairs (proabably in parts).
        * purchase dining table. (also proabably in part).
        * purchase plant
        * purchase painting

# Room 2 #

* Install American style Bosch® fridge freezer KGN36HI32;
* Bosch CTL636ES6 smart microwave; 
* Bosch smart Cooker with single oven HBG656RS6B; 
* Coffee makerCTL636EB6; 
* Bar stools

---- tasks done by you ----

## purchase items ##

### KGN36HI32 ###

* (url inside the <>)<https://www.bootskitchenappliances.com/product/kgn36hi32-bosch-serie-6-fridge-freezer-stainless-steel-40872-28.aspx?WT.z_PT=MDA&WT.z_MT=Affiliate&WT.z_MT=price4&WT.z_RTM=PHG&WT.z_RTM=PHG&WT.z_AT=fridge+freezers+frost+free&WT.z_AT=&WT.z_BR=Bosch&WT.z_FT=Free+Standing&WT.z_PC=KGN36HI32_IX&WT.srch=1&WT.srch=1&WT.z_CN=1101l82&WT.z_AG=Comparison&WT.z_KW=price4>
* price : 960 pounds sterling
* delivery: free

### CTL636ES6 ###
* <https://www.glotech.co.uk//appliances/product/bosch-ctl636es6-coffee-maker.html?callwinid=69394&ref=awin_glotech&awc=5783_1553566569_f33ee7f59b9c31fcfe2833b711a23c71>
* delivery: free depending on postcode.
* price : 1630 pounds sterling.


#### Alternatives to buy items ####

### KGN36HI32 ###

* <https://www.johnlewis.com/bosch-kgn36hi32-freestanding-fridge-freezer-with-home-connect-a-energy-rating-60cm-wide-silver/p2721753?s_kwcid=1dx43700023441499367&tmad=c&tmcampid=1&gclid=CjwKCAjw-OHkBRBkEiwAoOZql1fUDV-QXf5Ge-2xN1Pq1r4DAbQdueVZBLe_i-OVqYp0v79Nn6gnGxoCUC0QAvD_BwE&gclsrc=aw.ds>
* 979.99 pounds sterling

### CTL636ES6 ###
* <https://www.amazon.co.uk/dp/B01DDLFDE0/ref=nosim?tag=price4-21>
* price: 1373 
* delivery price: 31.58



# Room 3 #
*  Assemble/arrange all standalone furniture 
*  Assemble/arrange decorations, 
* assemble TV cabinet; 
* Install 50” Samsung 4K ultra HD smart TV; 
* fit 2 doors 0.91m x 2.6m each with SRF 20 dark one-way reflective window film.
# Room 4 #
* Assemble/arrange all standalone furniture and decorations;
* Fit 13’ x 13’ John Lewis and Partners® Cheriot Breed wool rich Heather 50oz twist snowdrop carpet; 
* Fit 90cm x 90cm blinds
# Room 5 #
* Assemble/arrange decorations 
* Set of Kogan® outdoor sun loungers
# Room 6 #
* Assemble/arrange all standalone furniture and decorations;
* Fit 16’ x 13’ John Lewis and Partners® Cheriot Breed wool rich Heather 50oz twist carpet, snowdrop; 
* Fit 90cm x 90cm blinds
# Room 7 #
* Install plumbing fixture; 
* install Wi-Fi enabled Siemens Smart 9kg, 
*  Install 1400rpm washing machine.

----- Buy products with warranty ---------
# Assumptions made #

1. The items being specified with an ID , are proabably not going to be in IKEA.
2. The items being specified with an ID universally have the same price from trusted sources.
3. unspecified items need to be brought at IKEA.
4. Because of the location and the nature of the building.

# Research about Lightbox #

## Peel Land and Property Group ##

* MediacityUK is a joint venture between Peel Land Property Group and Legal and General Capital.
* They have something to do with the development of this complex.